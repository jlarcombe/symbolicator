#!/usr/bin/python

from __future__ import print_function
import subprocess
import fileinput
import sys
import re
import optparse
import os


def architecture_for_code_type(code_type):
    arch_code_type_name = code_type.split()[0]
    code_types_to_architectures = {"X86": "i386", "X86-64": "x86_64", "PPC": "ppc", "ARM-64": "arm64", "ARM64": "arm64"}
    return code_types_to_architectures[arch_code_type_name]


recognized_crashlog_versions = ["6", "9", "10", "11", "12"]

recognized_hang_versions = ["7"]

recognized_spindump_versions = ["22", "26", "27", "28", "29", "32", "35", "35.1", "40", "44"]


def is_version_recognised(version, is_hang, is_spindump):
    if is_hang:
        return version in recognized_hang_versions
    elif is_spindump:
        return version in recognized_spindump_versions
    else:
        return version in recognized_crashlog_versions


def reformat_UUID(UUID):
    "Takes a plain-hex-number UUID, uppercases it, and inserts hyphens."
    UUID = UUID.upper()
    if len(UUID) == 36:
        # Already hyphenated.
        pass
    else:
        UUID = "-".join([UUID[0:8], UUID[8:12], UUID[12:16], UUID[16:20], UUID[20:]])
    return UUID


dSYM_cache = {}  # Keys: UUIDs; values: dSYM bundle paths (None indicating dSYM bundle not found)


def find_dSYM_by_UUID(UUID):
    try:
        dSYM_path = dSYM_cache[UUID]
    except KeyError:
        mdfind = subprocess.Popen(
            ["mdfind", "com_apple_xcode_dsym_uuids = " + reformat_UUID(UUID)], stdout=subprocess.PIPE
        )

        try:
            dSYM_path = next(iter(mdfind.stdout))[:-1]  # Strip \n
        except StopIteration:
            dSYM_path = None

        mdfind.wait()

        dSYM_cache[UUID] = dSYM_path

    return dSYM_path


def find_dSYM_and_start_address_by_bundle_ID(bundle_ID):
    if bundle_ID in binary_images:
        start_address, UUID = binary_images[bundle_ID]
        return (find_dSYM_by_UUID(UUID), start_address)
    elif bundle_ID.startswith("..."):
        bundle_ID_suffix = bundle_ID.lstrip("...")
        for (bundle_ID_key, (start_address, UUID)) in binary_images.iteritems():
            if bundle_ID_key.endswith(bundle_ID_suffix):
                binary_images[bundle_ID] = (start_address, UUID)
                return (find_dSYM_by_UUID(UUID), start_address)
        return (None, None)
    else:
        return (None, None)


def parse_binary_image_line(line):
    elements = iter(line.split())

    bundle_ID = ""
    UUID_in_brackets = ""
    filename = ""

    try:
        start_address = next(elements)
        next(elements)  # Hyphen-minus
        end_address = next(elements)
        bundle_ID = next(elements)

        # Sometimes we have no info at all, in which case there will be no versioning, just the UUID,
        # and the bundle version can have one element (eg. '7.0.0f23'), several (eg. 'compatibility 4.7.0'
        # or '6.2.0 - 6.2.0f88' in Lion), or not existing altogether in Mountain Lion - you can just have
        # a short version like this: (4.8). To keep things simple, iterate on until we find the start of
        # the UUID which is '<'.
        candidate = next(elements)
        while candidate[0] != "<":
            candidate = next(elements)
        if candidate[0] == "<":
            UUID_in_brackets = candidate

    except StopIteration:
        print("No UUID found for binary image " + line, file=sys.stderr)

    UUID = UUID_in_brackets.strip("<>")
    # The main(?) executable has plus sign before its bundle ID. Strip this off.
    bundle_ID = bundle_ID.lstrip("+")

    # the filename may contain spaces, so just take the string from after the end of the UUID
    # and strip off any leading whitspace
    filename = line[line.rfind(">") + 1 :].lstrip()
    leaf_name = os.path.basename(filename)

    return (bundle_ID, int(start_address, 16), UUID, leaf_name)


def get_dSYM_binary_path(dSYM_path):
    # otool doesn't know about .dSYM bundles. Rather than hardwire the relative path, get dwarfdump to find the binary.
    # This only works in 10.6 though - previous versions return the path to the bundle, so hardwire it if this fails.
    dSYM_binary_path = None
    dwarfdump = subprocess.Popen(["dwarfdump", "-u", dSYM_path], stdout=subprocess.PIPE)
    for bline in dwarfdump.stdout:
        line = bline.decode("utf-8")
        arch_pos = line.find(architecture)
        if arch_pos >= 0:
            path_pos = arch_pos + 1 + len(architecture)
            dSYM_binary_path = line[path_pos:].strip()
            break
    if dSYM_binary_path == dSYM_path:
        dSYM_name = os.path.split(dSYM_path.rstrip(os.sep))[1].split(".")[0]
        dSYM_binary_path = os.path.join(dSYM_path, "Contents/Resources/DWARF/", dSYM_name)
    return dSYM_binary_path


def calculate_slide(dSYM_path, start_address):
    slide = 0
    if dSYM_path and start_address:
        intended_start_address = start_address
        dSYM_binary_path = get_dSYM_binary_path(dSYM_path)
        if dSYM_binary_path:
            otool = subprocess.Popen(["otool", "-l", dSYM_binary_path], stdout=subprocess.PIPE)
            foundText = False
            for bline in otool.stdout:
                line = bline.decode("utf-8")
                if foundText:
                    elements = line.split()
                    if len(elements) >= 2 and elements[0] == "vmaddr":
                        intended_start_address = int(elements[1], 16)
                        break
                elif "__TEXT" in line:
                    foundText = True
            slide = start_address - intended_start_address
    return slide


def calculate_symbol_from_dwarfdump_info(
    function,
    decl_filename,
    decl_line_number,
    line_table_filename,
    line_table_line_number,
    c_plus_plus,
    specification_offset,
    found_mangled_name,
    dSYM_path,
    reached_inlines,
):
    line_number = decl_line_number
    filename = decl_filename
    if line_table_filename:
        # The line table is more reliable then AT_decl_file/AT_decl_line entries
        line_number = line_table_line_number
        filename = line_table_filename

    if c_plus_plus and specification_offset and not found_mangled_name:
        # Old versions of dwarfdump don't expand the AT_specification field, so we never see AT_name or AT_MIPS_linkage_name.
        # We want the latter really as this has the mangled name. Call dwarfdump again to retrieve the specification.
        dwarfdump = subprocess.Popen(
            [
                "dwarfdump",
                ("--arch=%s" % architecture),
                ("--debug-info=%s" % specification_offset),
                dSYM_path,
            ],
            stdout=subprocess.PIPE,
        )
        for bline in dwarfdump.stdout:
            line = bline.decode("utf-8").strip()
            if line.startswith("AT_MIPS_linkage_name("):
                function = " ".join(line.split()[1:-1]).strip('"')
                break
        else:
            dwarfdump.wait()

    if function and c_plus_plus and len(function) and function[0] != "-":
        function = (
            subprocess.Popen(["c++filt", "-n", function], stdout=subprocess.PIPE)
            .communicate()[0]
            .decode("utf-8")
            .strip()
        )

    format = None
    if function:
        if line_number >= 0:
            format = "%(function)s (%(filename)s:%(line_number)s)"
        elif filename:
            format = "%(function)s (%(filename)s)"
        else:
            format = "%(function)s"
    else:
        if line_number >= 0:
            format = "%(filename)s:%(line_number)s"
        elif filename:
            format = "%(filename)s"
        else:
            format = None

    if format is None:
        return ""

    this_func = format % {
        "function": function,
        "filename": filename,
        "line_number": line_number,
    }

    if reached_inlines:
        this_func = "inline " + this_func

    return this_func


def look_up_address_by_bundle_ID(bundle_ID, address, show_inlines):
    """ Returns an array, so that inline functions can be handled after their callers """
    symbols = []
    dSYM_path, start_address = find_dSYM_and_start_address_by_bundle_ID(bundle_ID)
    if dSYM_path:
        # calculate slide from dSYM's intended start address
        slide = calculate_slide(dSYM_path, start_address)

        address = hex(int(address, 16) - slide)

        dwarfdump = subprocess.Popen(["dwarfdump", "--lookup", address, dSYM_path], stdout=subprocess.PIPE)

        arch_string = (
            "%s)" % architecture
        )  # only match closing paren, to allow '(architecture i386)' on 10.5 and '(i386)' on 10.6
        c_plus_plus = False
        we_care = False
        tag_compile_unit = False
        tag_subprogram = False
        function = None
        decl_line_number = -1
        decl_filename = None
        line_table_line_number = -1
        line_table_filename = None
        specification_offset = 0
        found_mangled_name = False
        reached_inlines = False
        for bline in dwarfdump.stdout:
            line = bline.decode("utf-8").strip()
            if line.startswith("File: "):
                if arch_string in line:
                    we_care = True
                    tag_compile_unit = False
                    tag_subprogram = False
                else:
                    we_care = False
            elif we_care:
                if "TAG_compile_unit" in line:
                    tag_compile_unit = True
                    tag_subprogram = False
                elif "TAG_subprogram" in line:
                    tag_compile_unit = False
                    tag_subprogram = True
                elif (
                    line.startswith("AT_specification") and tag_subprogram
                ):  # fallback: on 10.5 this is the only thing with the name in that dwarfdump reads out
                    quoted_parts = line.split('"')
                    if len(quoted_parts) > 1 and not found_mangled_name:
                        function = quoted_parts[1]
                    openBrace = line.find("{")
                    if openBrace >= 0:
                        closeBrace = line.find("}", openBrace + 1)
                        if closeBrace > openBrace:
                            specification_offset = int(line[openBrace + 1 : closeBrace], 16)
                elif line.startswith("AT_decl_file") and not decl_filename and tag_subprogram:
                    # the first AT_decl_file one is the definition (cpp), the second is the declaration (header)
                    decl_filename = " ".join(line.split()[1:-1]).strip('"')
                    if decl_filename.find("/") >= 0:
                        decl_filename = decl_filename.rsplit("/")[-1]  # assume we only care about leaf name
                elif line.startswith("AT_decl_line") and decl_line_number < 0 and tag_subprogram:
                    decl_line_number = int(line.split()[1])  # assumes spaces around number
                elif line.startswith("AT_language"):
                    c_plus_plus = "plus_plus" in line  # this (correctly) catches Objective-C++ too
                elif line.startswith("AT_name("):
                    name = " ".join(line.split()[1:-1]).strip('"')
                    if tag_compile_unit:
                        filename = name
                    elif tag_subprogram and not found_mangled_name:
                        function = name
                elif (
                    c_plus_plus
                    and (line.startswith("AT_MIPS_linkage_name(") or line.startswith("AT_linkage_name("))
                    and tag_subprogram
                ):
                    # JL: the mangled name was stored in this field even on non-MIPS systems - apparently a de facto standard in DWARF.
                    # At some point they removed the out-of-date word 'MIPS' from the tag, so recognise both variants here.
                    # Use this in preference to AT_specification or AT_name because they may not have all the type information.
                    function = " ".join(line.split()[1:-1]).strip('"')
                    found_mangled_name = True
                elif line.startswith("Line table file: "):
                    match = re.search("'[^']+'", line)
                    if match:
                        line_table_filename = match.group(0).strip("'")
                    # The line number is the first decimal number after the filename.
                    match = re.search("[0-9]+", line[match.end(0) :])
                    if match:
                        line_table_line_number = int(match.group(0))
                elif "TAG_inlined_subroutine" in line:
                    if not show_inlines:
                        break

                    # work out the main calling symbol first, add it to our output array
                    # and clear the running variables ready for the inlined function
                    symbols.append(
                        calculate_symbol_from_dwarfdump_info(
                            function,
                            decl_filename,
                            decl_line_number,
                            line_table_filename,
                            line_table_line_number,
                            c_plus_plus,
                            specification_offset,
                            found_mangled_name,
                            dSYM_path,
                            reached_inlines,
                        )
                    )
                    function = None
                    decl_line_number = -1
                    decl_filename = None
                    line_table_line_number = -1
                    line_table_filename = None
                    specification_offset = 0
                    found_mangled_name = False
                    reached_inlines = True
        else:
            dwarfdump.wait()

        # do the last function we found
        symbols.append(
            calculate_symbol_from_dwarfdump_info(
                function,
                decl_filename,
                decl_line_number,
                line_table_filename,
                line_table_line_number,
                c_plus_plus,
                specification_offset,
                found_mangled_name,
                dSYM_path,
                reached_inlines,
            )
        )

        if symbols and not symbols[0]:
            # try atos instead. sometimes dwarfdump simply won't find the right
            # symbols and I can't work out why. we still prefer dwarfdump where
            # it works, though, because it gives better line number output
            dSYM_binary_path = get_dSYM_binary_path(dSYM_path)
            if dSYM_binary_path:
                symbols[0] = (
                    subprocess.Popen(
                        ["atos", "-o", dSYM_binary_path, "-arch", architecture, address],
                        stdout=subprocess.PIPE,
                    )
                    .communicate()[0]
                    .decode("utf-8")
                    .strip()
                )

        return symbols
    else:
        return []


def symbolicate_backtrace_line(line, show_inlines):
    address_pos = line.find("0x")
    if address_pos < 0:
        return line

    first_half = line[:address_pos]
    second_half = line[address_pos:]

    frame_and_bundle_ID_match = re.match(
        "(?P<frame_number>[0-9]+)\s+(?P<bundle_ID>[-_a-zA-Z0-9\./ ]+)", first_half
    )
    address_match = re.match("(?P<address>0x[0-9A-Fa-f]+)\s+", second_half)

    if not frame_and_bundle_ID_match and not address_match:
        return line

    bundle_ID = frame_and_bundle_ID_match.group("bundle_ID").strip()
    address = address_match.group("address")

    function_infos = look_up_address_by_bundle_ID(bundle_ID, address, show_inlines)
    if not function_infos:
        # try more sophisticated rules for finding the bundle ID
        resolved_bundle_ID = get_bundle_id_from_id_or_leafname(bundle_ID)
        if resolved_bundle_ID and resolved_bundle_ID != bundle_ID:
            function_infos = look_up_address_by_bundle_ID(resolved_bundle_ID, address, show_inlines)

    if not function_infos:
        return line
    else:
        lines = ""
        line_prefix = first_half
        for function_info in reversed(function_infos):  # because inline ones come later, in the debug info
            lines += line_prefix + function_info + "\n"
        return lines


def symbolicate_hang_callgraph_line(line):
    if not "0x" in line:
        return line  # no address to symbolise

    match = re.match(
        "(\D*?\d+)?(\s*)(?P<name>\S*)(\s*)(\(in )(?P<binary>.*)\)(\s*)(?P<unsymbolised_info>(\+\s*\d*\s*)?(load address 0x[0-9A-Fa-f]+\s*\+\s*0x[0-9A-Fa-f]+\s*)?)\[0x(?P<address>[0-9A-Fa-f]+)\].*$",
        line,
    )
    if not match:
        return line

    return handle_callgraph(
        line,
        match.group("binary"),
        match.group("address"),
        match.group("name"),
        match.group("unsymbolised_info"),
    )


def symbolicate_spindump_callgraph_line(line):
    if not "0x" in line:
        return line  # no address to symbolise

    match = re.match(
        "(\s*)(\W?\d+)(\s*)(?P<name>\S*)(\s*)(\+*)(\s*)(\d*)(\s*)(\()(?P<binary>.*)(?P<unsymbolised_info>\s\+.*\))(\s*)\[0x(?P<address>[0-9A-Fa-f]+)\].*$",
        line,
    )
    if not match:
        return line

    return handle_callgraph(
        line,
        match.group("binary"),
        match.group("address"),
        match.group("name"),
        match.group("unsymbolised_info"),
    )


def all_dots_appear_to_be_version_numbers(binary):
    has_dot = False
    for index in range(0, len(binary)):
        if binary[index] == ".":
            has_dot = True
            appears_to_be_version_number = False
            if index > 0 and index < len(binary) - 1:
                before = binary[index - 1]
                after = binary[index + 1]
                if str.isdigit(before) and str.isdigit(after):
                    appears_to_be_version_number = True

            if not appears_to_be_version_number:
                return False

    return has_dot


def get_bundle_id_from_id_or_leafname(binary_with_spaces):
    # in hangs and spindumps, the callgraph binary field isn't always an ID,
    # and this is also now true in crashes that are auto-converted from an
    # 'ips' file. if it has a dot in it, we assume it is a bundle ID, but if
    # it doesn't, we match against ends of binary images in our list.
    # if even that doesn't match, we match against the bundle leaf names.
    binary_with_spaces = binary_with_spaces.lower()
    binary = "".join(binary_with_spaces.split())  # remove spaces
    bundle_ID = binary
    if not "." in binary or all_dots_appear_to_be_version_numbers(binary):
        bundle_ID = next((s for s in binary_images.keys() if s.lower().endswith(binary)), None)
    if not bundle_ID and binary_with_spaces in binary_leafnames_to_bundle_IDs:
        bundle_ID = binary_leafnames_to_bundle_IDs[binary_with_spaces]
    return bundle_ID


def handle_callgraph(line, original_binary, address, name, unsymbolised_info):
    bundle_ID = get_bundle_id_from_id_or_leafname(original_binary)
    if not bundle_ID:
        return line

    function_infos = look_up_address_by_bundle_ID(bundle_ID, address, False)
    if not function_infos:
        return line
    else:
        return line.replace(name, function_infos[0]).replace(
            unsymbolised_info, ""
        )  # could support inline funcs here


def main():
    parser = optparse.OptionParser(
        usage="%prog [options] [files]",
        description="Reads one or more crash logs from named files or standard input, symbolicates them, and writes them to standard output.",
        version="%prog 6.6.14 by Peter Hosey",
    )
    parser.add_option(
        "--inlines",
        action="store_true",
        dest="inlines",
        help="show inlined subroutines for each address in callstack",
    )
    opts, args = parser.parse_args()

    global binary_images
    binary_images = {}  # Keys: bundle IDs; values: (start_address, UUID)s
    global binary_leafnames_to_bundle_IDs
    binary_leafnames_to_bundle_IDs = (
        {}
    )  # for binaries where the filename differs from the end of the bundle ID, maps from the lower-case leafname to the bundle ID
    global architecture
    architecture = None

    work = False
    is_hang = False
    is_spindump = False
    is_in_backtrace = False
    is_in_thread_state = False
    is_in_binary_images = False
    backtrace_lines = []
    thread_state_lines = []
    binary_image_lines = []
    thread_trace_start_exp = re.compile("^Thread \d+( Crashed)?:.*$")

    def flush_buffers():
        for line in backtrace_lines:
            if is_hang:
                sys.stdout.write(symbolicate_hang_callgraph_line(line))
            elif is_spindump:
                sys.stdout.write(symbolicate_spindump_callgraph_line(line))
            else:
                sys.stdout.write(symbolicate_backtrace_line(line, opts.inlines))
        for line in thread_state_lines:
            sys.stdout.write(line)
        for line in binary_image_lines:
            sys.stdout.write(line)
        del backtrace_lines[:]
        del thread_state_lines[:]
        del binary_image_lines[:]

    first_nonempty_line = True
    for line in fileinput.input(args):
        line_stripped = line.strip()
        if (not is_hang and not is_spindump and line_stripped.startswith("Process:")) or (
            first_nonempty_line
            and (
                line_stripped.startswith("Sampling")
                or line_stripped.startswith("Analysis of sampling")
                or line_stripped.startswith("Date/Time")
            )
        ):
            if is_in_binary_images:
                # End previous crash
                flush_buffers()
                is_in_binary_images = False

            # New crash
            work = True
            is_hang = line_stripped.startswith("Sampling") or line_stripped.startswith("Analysis of sampling")
            # crudely detect spindumps for now - would be better to wait for 'Data Source: Stackshots'
            # but that would require rejigging this whole loop and I don't have time at present
            is_spindump = line_stripped.startswith("Date/Time")
            is_in_backtrace = False
            is_in_thread_state = False
            is_in_binary_images = False
            backtrace_lines = []
            thread_state_lines = []
            binary_image_lines = []
            sys.stdout.write(line)
        elif not work:
            continue
        elif line_stripped.startswith("Report Version:"):
            version = line_stripped[len("Report Version:") :].strip()
            if not is_version_recognised(version, is_hang, is_spindump):
                print("Unrecognized report version:", version, "(skipping this report)", file=sys.stderr)
                work = False
            sys.stdout.write(line)
        elif line_stripped.startswith("Code Type:"):
            architecture = architecture_for_code_type(line_stripped[len("Code Type:") :].strip())
            sys.stdout.write(line)
        elif line_stripped.startswith("Architecture:"):
            architecture = line_stripped[len("Architecture:") :].split()[0].strip()
            sys.stdout.write(line)
        elif (
            (is_hang and line_stripped.startswith("Call graph:"))
            or (is_spindump and line_stripped.startswith("Thread "))
            or (is_spindump and line_stripped.startswith("Heaviest "))
            or (is_spindump and line_stripped.startswith("Power Source:"))
            or (not is_hang and not is_spindump and thread_trace_start_exp.match(line_stripped))
        ):
            is_in_backtrace = True
            backtrace_lines.append(line)
        elif is_in_backtrace and ("Thread State" in line_stripped):
            is_in_backtrace = False
            is_in_thread_state = True
            thread_state_lines.append(line)
        elif line_stripped.startswith("Binary Images"):
            is_in_backtrace = False
            is_in_thread_state = False
            is_in_binary_images = True
            binary_image_lines.append(line)
        elif is_in_thread_state:
            thread_state_lines.append(line)
        elif is_in_backtrace:
            backtrace_lines.append(line)
        elif not is_in_binary_images:
            # We haven't gotten to backtrace or binary images yet. Pass this line through.
            sys.stdout.write(line)
        elif is_in_binary_images:
            if line_stripped.strip() and line_stripped.startswith("0x"):
                binary_image_lines.append(line)
                bundle_ID, start_address, UUID, leaf_name = parse_binary_image_line(line_stripped)
                binary_images[bundle_ID] = (start_address, UUID)
                if not bundle_ID.lower().endswith(leaf_name.lower()):
                    binary_leafnames_to_bundle_IDs[leaf_name.lower()] = bundle_ID
            else:
                # End of crash
                flush_buffers()
                is_in_binary_images = False
        if line_stripped:
            first_nonempty_line = False

    if is_in_binary_images:
        # Crash not followed by a newline
        flush_buffers()


if __name__ == "__main__":
    main()

